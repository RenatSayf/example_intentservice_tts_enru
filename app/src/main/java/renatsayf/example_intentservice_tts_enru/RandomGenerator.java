package renatsayf.example_intentservice_tts_enru;

import java.util.BitSet;
import java.util.Random;

/**
 * Created by Renat on 05.02.2017.
 */

public class RandomGenerator
{
    private final BitSet input;
    private final Random rnd;
    private final int Count;
    private int genCount=0;
    private int start = 0;
    public RandomGenerator(int in, int seed)
    {
        Count=in;
        rnd=new Random(in);
        rnd.setSeed(seed);
        input = new BitSet(in);
    }

    public RandomGenerator(int start, int in, int seed)
    {
        this.start = start;
        Count=in;
        rnd=new Random(in);
        rnd.setSeed(seed);
        input = new BitSet(in);
    }

    public int generate()
    {
        if (genCount>=Count)
            return -1;

        int next;
        do
        {
            next = rnd.nextInt(Count);
        }
        while (input.get(next));
        input.set(next);
        genCount++;
        return next + start;
    }
}

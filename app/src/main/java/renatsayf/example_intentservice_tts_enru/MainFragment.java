package renatsayf.example_intentservice_tts_enru;


import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements speechService.IStartStopService
{
    private View fragment_view = null;
    private TextView textViewEn, textViewRu;
    private Button buttonPlay, buttonPause, buttonStop;
    private Chronometer chronometer;
    private long startTime;
    private Spinner spinner;
    private int spinnerOldIndex = 0;
    private Intent speechIntentService; // TODO: Intent обьявление
    private UpdateBroadcastReceiver mUpdateBroadcastReceiver;

    private String KEY_ENG_TEXT = "key_eng_text";
    private String KEY_RUS_TEXT = "key_rus_text";
    private String KEY_SPINN_INDEX = "key_spinn_index";
    private String KEY_SPINN_OLD_INDEX = "key_spinn_old_index";
    private String KEY_START_TIME = "key_start_time";
    private String KEY_BTN_PLAY_VISIBLE = "key_btn_play_visible";
    private String KEY_BTN_PAUSE_VISIBLE = "key_btn_pause_visible";
    private String KEY_BTN_STOP_VISIBLE = "key_btn_stop_visible";


    public MainFragment()
    {

    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        try
        {
            spinnerOldIndex = spinner.getSelectedItemPosition();
            outState.putInt(KEY_SPINN_INDEX, spinner.getSelectedItemPosition());
            outState.putInt(KEY_SPINN_OLD_INDEX, spinnerOldIndex);
            outState.putString(KEY_ENG_TEXT, textViewEn.getText().toString());
            outState.putString(KEY_RUS_TEXT, textViewRu.getText().toString());
            outState.putInt(KEY_BTN_PLAY_VISIBLE, buttonPlay.getVisibility());
            outState.putInt(KEY_BTN_PAUSE_VISIBLE, buttonPause.getVisibility());
            outState.putInt(KEY_BTN_STOP_VISIBLE, buttonStop.getVisibility());
            outState.putLong(KEY_START_TIME, startTime);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //region // TODO: BroadcastReceiver registerReceiver
        mUpdateBroadcastReceiver = new UpdateBroadcastReceiver();
        IntentFilter updateIntentFilter = new IntentFilter(speechService.ACTION_UPDATE);
        updateIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        try
        {
            getActivity().registerReceiver(mUpdateBroadcastReceiver, updateIntentFilter);
        } catch (Exception e)
        {
            Log.e(getString(R.string.key_log_msg), e.getMessage());
        }
        //endregion

        speechService.setStartStopListeners(MainFragment.this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        fragment_view = inflater.inflate(R.layout.fragment_main, container, false);

        textViewEn = (TextView) fragment_view.findViewById(R.id.text_view_en);
        textViewRu = (TextView) fragment_view.findViewById(R.id.text_view_ru);
        chronometer = (Chronometer) fragment_view.findViewById(R.id.chronometer);
        chronometer.start();

        spinner = (Spinner) fragment_view.findViewById(R.id.spinner);

        buttonPlay = (Button) fragment_view.findViewById(R.id.btn_play);
        buttonPause = (Button) fragment_view.findViewById(R.id.btn_pause);
        buttonPause.setVisibility(View.GONE);
        buttonStop = (Button) fragment_view.findViewById(R.id.btn_stop);

        if (savedInstanceState == null)
        {
            startTime = chronometer.getBase();
        }

        if (savedInstanceState != null)
        {
            spinner.setSelection(savedInstanceState.getInt(KEY_SPINN_INDEX));
            spinnerOldIndex = savedInstanceState.getInt(KEY_SPINN_OLD_INDEX);
            textViewEn.setText(savedInstanceState.getString(KEY_ENG_TEXT));
            textViewRu.setText(savedInstanceState.getString(KEY_RUS_TEXT));
            buttonPlay.setVisibility(savedInstanceState.getInt(KEY_BTN_PLAY_VISIBLE));
            buttonPause.setVisibility(savedInstanceState.getInt(KEY_BTN_PAUSE_VISIBLE));
            buttonStop.setVisibility(savedInstanceState.getInt(KEY_BTN_STOP_VISIBLE));
            startTime = savedInstanceState.getLong(KEY_START_TIME);
            chronometer.setBase(Calendar.SECOND + startTime);
        }

        buttons_OnClick();
        spinner_OnItemSelectedListener();

        speechIntentService = new Intent(getActivity(), speechService.class); // TODO: Intent создание экзепляра

        return fragment_view;
    }

    private void spinner_OnItemSelectedListener()
    {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l)
            {
                if (spinnerOldIndex == index) return;
                speechService.stopIntentService();
                speechIntentService.putExtra(getString(R.string.key_play_order), index);
                spinnerOldIndex = index;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        // TODO: BroadcastReceiver unregisterReceiver
        getActivity().unregisterReceiver(mUpdateBroadcastReceiver);
    }

    private void buttons_OnClick()
    {
        buttonPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(getActivity(), R.string.text_to_toast_play, Toast.LENGTH_SHORT).show();
                speechIntentService.putExtra(getString(R.string.key_play_order), spinner.getSelectedItemPosition());
                getActivity().startService(speechIntentService);
            }
        });

        buttonPause.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(getActivity(), R.string.text_to_toast_pause, Toast.LENGTH_SHORT).show();
                speechService.stopIntentService();
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(getActivity(), R.string.text_to_toast_stop, Toast.LENGTH_SHORT).show();
                speechService.stopIntentService();
                speechService.resetCount();
            }
        });
    }

    @Override
    public void onStartSpeechService()
    {
        buttonPlay.setVisibility(View.GONE);
        buttonPause.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopSpeechService()
    {
        buttonPlay.setVisibility(View.VISIBLE);
        buttonPause.setVisibility(View.GONE);
    }

    //region // TODO: BroadcastReceiver class
    public class UpdateBroadcastReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            textViewEn.setText(intent.getStringExtra(speechService.EXTRA_KEY_UPDATE_EN));
            textViewRu.setText(intent.getStringExtra(speechService.EXTRA_KEY_UPDATE_RU));
        }
    }
    //endregion

}

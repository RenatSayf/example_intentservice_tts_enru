package renatsayf.example_intentservice_tts_enru;

import android.app.IntentService;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

// TODO: Класс speechService - наследник IntentService
public class speechService extends IntentService
{
    public static final String ACTION_UPDATE = "renatsayf.example_intentservice_tts_enru";
    public static final String EXTRA_KEY_UPDATE_EN = "EXTRA_UPDATE_EN";
    public static final String EXTRA_KEY_UPDATE_RU = "EXTRA_UPDATE_RU";

    private static boolean stop = false;
    private static int dictIndex = 0;
    private static int wordIndex = 0;
    public static TextToSpeech referens;

    private Intent updateIntent;
    private ArrayList<ArrayList<Word>> playList;
    private String textEn;
    private String textRu;
    private HashMap<String,String> hashMap = new HashMap<>();
    private TextToSpeech speech;
    private static RandomGenerator wordIndexGen;

    private static IStartStopService iStartStopService;
    public static void setStartStopListeners(IStartStopService listeners)
    {
        iStartStopService = listeners;
    }

    public interface IStartStopService
    {
        void onStartSpeechService();
        void onStopSpeechService();
    }

    public speechService()
    {
        super("example_intentservice_tts_enru");
    }

    public static void setSpeechReferens(TextToSpeech speech_ref)
    {
        speechService.referens = speech_ref;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        speech = referens;
        ArrayList<Word> kit1 = new ArrayList<>();
        kit1.add(new Word("good", "хорошо"));
        kit1.add(new Word("add", "добавить"));
        kit1.add(new Word("select", "выбрать"));
        kit1.add(new Word("run", "запустить"));

        ArrayList<Word> kit2 = new ArrayList<>();
        kit2.add(new Word("green", "зеленый"));
        kit2.add(new Word("red", "красный"));
        kit2.add(new Word("black", "черный"));
        kit2.add(new Word("grey", "серый"));

        ArrayList<Word> kit3 = new ArrayList<>();
        kit3.add(new Word("one", "один"));
        kit3.add(new Word("two", "два"));
        kit3.add(new Word("three", "три"));

        playList = new ArrayList<>();
        playList.add(kit1);
        playList.add(kit2);
        playList.add(kit3);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        stop = false;
        if (iStartStopService != null)
        {
            iStartStopService.onStartSpeechService();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {

        super.onDestroy();
    }
    public static void stopIntentService()
    {
        stop = true;
        if (iStartStopService != null)
        {
            iStartStopService.onStopSpeechService();
        }
    }

    int wordsQuantity;
    @Override
    protected void onHandleIntent(Intent intent)
    {
        int play_order = intent.getIntExtra(getString(R.string.key_play_order), 0);
        updateIntent = new Intent();
        updateIntent.setAction(ACTION_UPDATE);
        updateIntent.addCategory(Intent.CATEGORY_DEFAULT);

        if (play_order == 1 && wordIndex == 0 && dictIndex == 0)
        {
            dictIndex = playList.size()-1;
            wordIndex = playList.get(dictIndex).size()-1;
        }

        if (play_order == 2)
        {
            for (int i = 0; i < playList.size(); i++)
            {
                wordsQuantity += playList.get(i).size();
            }
            Date date = new Date();
            wordIndexGen = new RandomGenerator(wordsQuantity, (int) date.getTime());
            wordIndex = wordIndexGen.generate();
        }

        playWords(wordIndex, play_order);
        while (!stop) // пока stop = false, выполняется код в onHandleIntent
        {
            try
            {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void resetCount()
    {
        dictIndex = 0; // Сброс индексов списков и элементов
        wordIndex = 0;
    }

    private void playWords(int word_index, final int order)
    {
        switch (order)
        {
            case 0: // Прямое воспроизведение слов
                if (word_index >= playList.get(dictIndex).size())
                {
                    // Если индекс элемента текущего списка >= количества эл. в этом списке, счетчик эл. обнуляем, индекс списка увелич на 1
                    wordIndex = 0;
                    dictIndex++;
                }
                if (dictIndex >= playList.size())
                {
                    // Если индекс списка >= общего колич. списков - индекс списка обнуляем
                    wordIndex = 0;
                    dictIndex = 0;
                }
                break;
            case 1: // Обратное воспроизведение слов
                if (wordIndex < 0 && dictIndex < 0)
                {
                    dictIndex = playList.size() - 1;
                    wordIndex = playList.get(dictIndex).size() - 1;
                }
                if (word_index < 0)
                {
                    dictIndex--;
                    if (dictIndex >= 0)
                    {
                        wordIndex = playList.get(dictIndex).size()-1;
                    }
                }
                if (dictIndex < 0)
                {
                    dictIndex = playList.size()-1;
                    wordIndex = playList.get(dictIndex).size()-1;
                }
                break;
            case 2: // Случайное воспроизведение слов
                Date date = new Date();
                if (wordIndex < 0)
                {
                    wordIndexGen = new RandomGenerator(wordsQuantity, (int) date.getTime());
                    wordIndex = wordIndexGen.generate();
                }

                int i = 0;
                while (i < playList.size())
                {
                    if (wordIndex >= playList.get(i).size())
                    {
                        wordIndex = wordIndex - playList.get(i).size();
                        i++;
                    }
                    else
                    {
                        dictIndex = i;
                        break;
                    }
                }
                break;
            default:break;
        }

        final int tempListIndex = dictIndex;
        final int tempWordIndex = wordIndex;
        speech.setOnUtteranceProgressListener(new UtteranceProgressListener()
        {
            @Override
            public void onStart(String utteranceId)
            {
                if (stop) return;
                updateIntent.putExtra(EXTRA_KEY_UPDATE_EN, textEn);
                updateIntent.putExtra(EXTRA_KEY_UPDATE_RU, textRu);
                sendBroadcast(updateIntent);
            }

            @Override
            public void onDone(String utteranceId)
            {
                if (utteranceId.equals("en"))
                {
                    String text = playList.get(tempListIndex).get(tempWordIndex).getRussian();
                    speakWord(text);
                }
                if (utteranceId.equals("ru"))
                {
                    if (stop) return;
                    switch (order)
                    {
                        case 0:
                            wordIndex++;
                            break;
                        case 1:
                            wordIndex--;
                            break;
                        case 2:
                            wordIndex = wordIndexGen.generate();
                            break;
                        default:break;
                    }

                    playWords(wordIndex, order);
                }
                return;
            }

            @Override
            public void onError(String utteranceId)
            {

            }
        });
        String english = playList.get(dictIndex).get(wordIndex).getEnglish();
        speakWord(playList.get(dictIndex).get(wordIndex).getEnglish());
    }

    private void speakWord(String text)
    {
        if (stop) return;
        String[] langOfText = getLangOfText(text);
        if (langOfText[1].equals("en"))
        {
            textEn = text;
            textRu = "";
            speech.setLanguage(Locale.US);
            hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "en");
        }
        else if (langOfText[1].equals("ru"))
        {
            textRu = text;
            speech.setLanguage(Locale.getDefault());
            hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "ru");
        }
        else if (langOfText[1] == null)
        {
            Toast.makeText(this,getString(R.string.text_msg_not_match_lang) + text, Toast.LENGTH_SHORT).show();
            return;
        }
        speech.speak(text, TextToSpeech.QUEUE_ADD, hashMap);
    }

    public String[] getLangOfText(String text)
    {
        String str = text;
        String[] lang = new String[2];
        int char_first;
        for (int i = 0; i < str.length(); i++)
        {
            char_first = str.codePointAt(i);
            if ((char_first >= 33 && char_first <= 64) || (str.codePointAt(i) >= 91 && str.codePointAt(i) <= 96) || (str.codePointAt(i) >= 123 && str.codePointAt(i) <= 126))
            {
                continue;
            }
            if (str.codePointAt(i) >= 1025 && str.codePointAt(i) <= 1105)
            {
                lang[0] = "ru-en";
                lang[1] = "ru";
            }
            else if (str.codePointAt(i) >= 65 && str.codePointAt(i) <= 122)
            {
                lang[0] = "en-ru";
                lang[1] = "en";
            }
            else
            {
                lang[0] = null;
                lang[1] = null;
            }
        }
        return lang;
    }
}

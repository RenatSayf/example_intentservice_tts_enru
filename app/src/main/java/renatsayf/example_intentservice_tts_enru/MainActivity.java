package renatsayf.example_intentservice_tts_enru;

import android.app.ActivityManager;
import android.app.FragmentTransaction;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
{
    MainFragment mainFragment;
    FragmentTransaction transaction;
    public static TextToSpeech speechReferens;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null)
        {
            speechReferens = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener()
            {
                @Override
                public void onInit(int status)
                {
                    if (status == TextToSpeech.SUCCESS)
                    {
                        int resultEn = speechReferens.isLanguageAvailable(Locale.US);
                        if (resultEn == TextToSpeech.LANG_MISSING_DATA || resultEn == TextToSpeech.LANG_NOT_SUPPORTED)
                        {
                            Intent installTTSdata = new Intent();
                            installTTSdata.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                        } else
                        {
                            speechReferens.setLanguage(Locale.US);
                            // TODO: TextToSpeech Передача ссылки на экземпляр TextToSpeech в speechService
                            speechService.setSpeechReferens(speechReferens);
                        }
                    } else
                    {
                        Intent instTTSengine = new Intent(Intent.ACTION_VIEW);
                        instTTSengine.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.tts&hl=ru"));
                    }
                }
            });
        }

        if (savedInstanceState == null) // TODO: Fragment Проверка - что бы фрагмент не создавался каждый раз при повороте экрана
        {
            mainFragment = new MainFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_frame, mainFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (!isActivityOnTop())
        {
            speechService.stopIntentService();
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (!isActivityOnTop())
        {
            speechService.stopIntentService();
        }
    }

    // TODO: ActivityManager.RunningAppProcessInfo Проверка, что активити находится на верху стека
    public boolean isActivityOnTop()
    {
        final ActivityManager activityManager = (ActivityManager) getSystemService(Service.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses.size() > 0)
        {
            String processName = runningAppProcesses.get(0).processName;
            String packageName = getApplicationInfo().packageName;
            if (processName.equals(packageName))
            {
                return true;
            }
        }
        return false;
    }
}

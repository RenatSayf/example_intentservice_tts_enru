package renatsayf.example_intentservice_tts_enru;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ренат on 30.01.2017.
 */

public class Word extends ArrayList
{
    private String english;
    private String russian;

    public Word(String english, String russian)
    {
        this.english = english;
        this.russian = russian;
    }

    public String getEnglish()
    {
        return english;
    }

    public void setEnglish(String english)
    {
        this.english = english;
    }

    public String getRussian()
    {
        return russian;
    }

    public void setRussian(String russian)
    {
        this.russian = russian;
    }

    @Override
    public int size()
    {
        return super.size();
    }
}
